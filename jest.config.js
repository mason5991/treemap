module.exports = {
  verbose: true,
  coverageDirectory: './reports/jest/coverage',
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  testPathIgnorePatterns: ['/build/'],
  // roots: ['tests/jest'],
  testMatch: [
    '<rootDir>/tests/jest/**/*.test.ts',
    '<rootDir>/tests/jest/**/*.test.tsx',
  ],
  transform: {
    '^.+\\.(js|jsx|ts|tsx)$': 'ts-jest',
  },
  globals: {
    'ts-jest': {
      tsconfig: {
        // allow js in typescript
        allowJs: true,
      },
    },
  },
  collectCoverageFrom: [
    'src/**/*.{js,jsx,ts,tsx}',
    '!**/node_modules/**',
    '!**/vendor/**',
  ],
  reporters: [
    'default',
    [
      'jest-html-reporters',
      {
        publicPath: 'reports/jest',
        filename: 'jest-report.html',
        expand: true,
      },
    ],
  ],
};
