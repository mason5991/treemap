import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import userEvent from '@testing-library/user-event';
import Home from '../../../src/containers/Home';

describe('Load Treemap', () => {
  test('Should display treemap if success', async () => {
    const wrapper = render(<Home />);
    const testData =
      "[{ name: 'A', weight: 3, value: -0.02 },{ name: 'B', weight: 3, value: 0.05 },{ name: 'C', weight: 6, value: 0.015 },{ name: 'D', weight: 2, value: -0.01 },{ name: 'E', weight: 3, value: 0.01 }]";
    const numOfRow = '3';
    const testDataInput = wrapper.queryByTestId(
      'input-data',
    ) as HTMLInputElement;
    const numOfRowInput = wrapper.queryByTestId(
      'input-numOfRow',
    ) as HTMLInputElement;
    // const rect = wrapper.queryByTestId('rect') as HTMLDivElement;
    userEvent.type(testDataInput, testData);
    expect(testDataInput.value).toBe(testData);
    userEvent.type(numOfRowInput, numOfRow);
    expect(numOfRowInput.value).toBe(numOfRow);
    fireEvent.click(screen.getByText('Submit'));
    const container = wrapper.queryByTestId('rect-container');
    const newJson = testData.replace(/([a-zA-Z0-9]+?):/g, '"$1":');
    const arrData = JSON.parse(newJson.replace(/'/g, '"'));
    arrData.forEach((d, index) => {
      expect(container).toContainElement(
        wrapper.queryByTestId(`rect-name-${index}`),
      );
    });
  });
});
