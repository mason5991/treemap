import { makeStyles } from '@material-ui/core';
import Home from './containers/Home';
import './App.css';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginTop: 64,
  },
}));

function App() {
  const classes = useStyles();
  return (
    <div className="App">
      <div className={classes.content}>
        <Home />
      </div>
    </div>
  );
}

export default App;
