import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'inline-block',
    textAlign: 'center',
    height: '96px',
    borderRadius: '16px',
    margin: '4px',
  },
}));

const Rectangle = ({ name, value, width, index }) => {
  const classes = useStyles();
  return (
    <div
      style={{
        width: `calc(${width} - 8px)`,
        background: value < 0 ? '#d67474' : '#97c17a',
      }}
      className={classes.root}
      data-testid="rect"
    >
      <div data-testid={`rect-name-${index}`}>{name}</div>
      <div>{`${value}%`}</div>
    </div>
  );
};

Rectangle.propTypes = {
  name: PropTypes.string,
  value: PropTypes.number,
  width: PropTypes.string,
  index: PropTypes.number,
};

Rectangle.defaultProps = {
  name: '',
  value: '',
  width: '100%',
  index: 0,
};

export default Rectangle;
