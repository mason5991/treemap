import { useState, useEffect } from 'react';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Rectangle from '../components/Rectangle';

const testData = [
  { name: 'A', weight: 3, value: -0.02 },
  { name: 'B', weight: 3, value: 0.05 },
  { name: 'C', weight: 6, value: 0.015 },
  { name: 'D', weight: 2, value: -0.01 },
  { name: 'E', weight: 3, value: 0.01 },
];

interface TestData {
  name: string;
  weight: number;
  value: number;
}

interface RectData extends TestData {
  width: string;
}

const compare = (a, b) => {
  // console.log('a:', a);
  // console.log('b:', b);
  return b.weight - a.weight;
};

const Home = () => {
  // "[{ name: 'A', weight: 3, value: -0.02 },{ name: 'B', weight: 3, value: 0.05 },{ name: 'C', weight: 6, value: 0.015 },{ name: 'D', weight: 2, value: -0.01 },{ name: 'E', weight: 3, value: 0.01 }]"
  const [data, setData] = useState('');
  const [numOfRow, setNumOfRow] = useState('');
  const [rect, setRect] = useState<RectData[]>([]);
  const onSubmit = () => {
    let newJson = data.replace(/([a-zA-Z0-9]+?):/g, '"$1":');
    newJson = newJson.replace(/'/g, '"');
    const newData = JSON.parse(newJson).sort(compare);
    let total = 0;
    newData.forEach((d) => {
      total += d.weight;
    });
    const rows = parseInt(numOfRow, 10);
    const max = Math.ceil(total / rows);
    const displayData = newData.map((d) => ({
      name: d.name,
      weight: d.weight,
      value: Math.round((d.value * 100 + Number.EPSILON) * 100) / 100,
      width: `${(100 / max) * d.weight > 100 ? 100 : (100 / max) * d.weight}%`,
    }));
    setRect(displayData);
  };

  return (
    <Grid container spacing={1}>
      <Grid item xs={12} md={4}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <TextField
              multiline
              rows={10}
              name="data"
              value={data}
              onChange={(e) => setData(e.target.value)}
              label="Input Data"
              variant="outlined"
              color="primary"
              fullWidth
              inputProps={{
                'data-testid': 'input-data',
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              type="text"
              name="numOfRow"
              value={numOfRow}
              onChange={(e) => setNumOfRow(e.target.value)}
              variant="outlined"
              label="Number of Row"
              color="primary"
              fullWidth
              inputProps={{
                'data-testid': 'input-numOfRow',
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <Button
              type="button"
              onClick={onSubmit}
              variant="contained"
              color="primary"
            >
              Submit
            </Button>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} md={8}>
        <div data-testid="rect-container">
          {rect.map((d, index) => (
            <Rectangle
              key={`${d.name}-${index}`}
              name={d.name}
              value={d.value}
              width={d.width}
              index={index}
            />
          ))}
        </div>
      </Grid>
    </Grid>
  );
};

export default Home;
