# Treemap

## Installation

```
npm install
```

## Unit test

```
npm run test
```

## Build

```
npm run build
```

## Start application

```
npm run start
```

1. Run on http;//localhost:3000

2. Input JSON data and number of row

   Example:

   Input data: [{ name: 'A', weight: 3, value: -0.02 },{ name: 'B', weight: 3, value: 0.05 },{ name: 'C', weight: 6, value: 0.015 },{ name: 'D', weight: 2, value: -0.01 },{ name: 'E', weight: 3, value: 0.01 }]

   Number of row: 3

3. Click Submit button
